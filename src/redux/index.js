import {createStore, applyMiddleware} from 'redux'
import thunkMiddleware from 'redux-thunk'
import * as firebase from 'firebase'

const initialState = {
  data: {},
  isLoading: false,
}

const reducer = (state = initialState, action) => {
  switch (action.type){
    case 'SET_DATA':
      return{
        ...state,
        data: action.value
      }
    case 'SET_LOADING':
      return{
        ...state,
        isLoading: action.value
      }
    default:
      return state
  }
}

export const store = createStore(reducer, applyMiddleware(thunkMiddleware))
