import * as firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyBeLmJLJFsK99BMD6T3zKpnAkTwmEry_s4",
  authDomain: "remedi-da9fd.firebaseapp.com",
  databaseURL: "https://remedi-da9fd.firebaseio.com",
  projectId: "remedi-da9fd",
  storageBucket: "remedi-da9fd.appspot.com",
  messagingSenderId: "201816869004",
  appId: "1:201816869004:web:1696bc83269741608a93a9",
  measurementId: "G-87WY7QBS0L"
};

const Firebase = firebase.initializeApp(firebaseConfig)

export default Firebase