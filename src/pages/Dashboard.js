import React, {Component} from 'react'
import {View, Text, Button} from 'react-native'

import {connect} from 'react-redux'
import Firebase from '../config/Firebase'

class Dashboard extends Component{
  handleSignout = () => {
    Firebase.auth().signOut()
    this.props.navigation.navigate('Login')
  }
  render(){
    return(
      <View style = {{alignItems: 'center', justifyContent: 'center'}}>
        <Text>Dashboard</Text>
        <Text>Welcome {this.props.data.uid}</Text>
        <Button title = 'Logout' onPress = {this.handleSignout} />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.data
  }
}

export default connect(mapStateToProps, null)(Dashboard)

