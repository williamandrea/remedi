import React, {Component} from 'react'
import {View, Text, TouchableOpacity} from 'react-native'

import styles from '../../styles'

export default class Home extends Component {
  render(){
    return (
      <View style = {styles.container}>
        <Text style = {styles.h1}>Remind Me Deadline</Text>
        <TouchableOpacity style = {[styles.button, styles.btnPrimary]} onPress = {() => this.props.navigation.navigate('Login')}>
          <Text style = {{fontSize: 20, color: '#ecf0f1'}}>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity style = {[styles.button, styles.btnSecondary]} onPress = {() => this.props.navigation.navigate('Register')}>
          <Text style = {{fontSize: 20, color: '#ecf0f1'}}>Register</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
