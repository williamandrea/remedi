import React, {Component} from 'react'
import {View, Text, TextInput, TouchableOpacity, Alert} from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'

import Firebase from '../config/Firebase'
import {connect} from 'react-redux'

import styles from '../../styles'

class Login extends Component{
  state = {
    email: '',
    password: '',
  }

  handleSubmit = async () => {
    this.props.setLoading(true)
    const {email, password} = this.state
    const response = await Firebase.auth().signInWithEmailAndPassword(email, password)
    .then((res) => {
      console.log(res.user.uid)
      this.props.setData({uid: res.user.uid})
      this.props.navigation.navigate('Dashboard')
    })
    .catch((err) => {
      console.log(err)
      alert('Incorrect username or password.')
    })
    this.props.setLoading(false)
  }

  render(){
    const {email, password} = this.state
    return (
      <View style = {styles.container}>
        <Spinner visible = {this.props.isLoading} style = {{color: '#FFF'}}/>
        <Text style = {styles.h1}>Login</Text>
        <TextInput style = {styles.input} placeholder = "Email" autoCapitalize = 'none'
          value = {email} 
          onChangeText = {(email) => this.setState({email})} />
        <TextInput style = {styles.input} placeholder = "Password" secureTextEntry 
          value = {password} 
          onChangeText = {(password) => this.setState({password})}/>
        <TouchableOpacity style = {[styles.button, styles.btnPrimary]} onPress = {this.handleSubmit}>
          <Text style = {{fontSize: 20, color: '#ecf0f1'}}>Sign In</Text>
        </TouchableOpacity>
        <TouchableOpacity style = {[styles.button, styles.btnSecondary]} onPress = {() => this.props.navigation.navigate('Home')}>
          <Text style = {{fontSize: 20, color: '#ecf0f1'}}>Cancel</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoading,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setLoading: (val) => dispatch({type: 'SET_LOADING', value: val}),
    setData: (val) => dispatch({type: 'SET_DATA', value: val})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
