import {StyleSheet, Dimensions} from 'react-native'

const {height, width} = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h1: {
    fontSize: 50,
    marginBottom: 50,
    color: '#8e44ad',
    marginHorizontal: 20,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  button: {
    borderBottomLeftRadius: 25,
    width: width * 0.9, 
    height: height * 0.09,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  input: {
    borderTopRightRadius: 25,
    width: width * 0.9, 
    height: height * 0.09,
    fontSize: 15,
    fontFamily: 'consolas',
    backgroundColor: '#ecf0f1',
    marginBottom: 10,
    paddingHorizontal: 20,
    marginHorizontal: 20,
  },
  btnPrimary: {
    backgroundColor: '#8e44ad',
  },
  btnSecondary: {
    backgroundColor: '#95a5a6',
  }
})

export default styles