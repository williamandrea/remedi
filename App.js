import React from 'react'
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'

import {Provider} from 'react-redux'
import {store} from './src/redux'

import Home from './src/pages/Home'
import Login from './src/pages/Login'
import Register from './src/pages/Register'
import Dashboard from './src/pages/Dashboard'

const Stack = createStackNavigator()

export default App = () => {
  return (
    <Provider store = {store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName = "Home" screenOptions = {{headerShown: false}}>
          <Stack.Screen name = "Home" component = {Home} />
          <Stack.Screen name = "Login" component = {Login} />
          <Stack.Screen name = "Register" component = {Register} />
          <Stack.Screen name = "Dashboard" component = {Dashboard} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}